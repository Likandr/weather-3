import 'dart:async';
import 'package:flutter/material.dart';

import 'package:weather_3/data/api.dart';
import 'city_add_item.dart';
import '../../data/city/city_model.dart';

class SearchPage extends StatefulWidget {
  const SearchPage({
    Key key,
  }) : super(key: key);

  @override
  _SearchPageState createState() => _SearchPageState();
}

class _SearchPageState extends State<SearchPage> {
  static const EdgeInsets _padding =
      const EdgeInsets.symmetric(horizontal: 8.0, vertical: 8.0);

  final _cities = <CityTile>[];
  final TextEditingController _controller = TextEditingController();

  Future<void> searchCities(String searchText) async {
    if (searchText.length > 3) {
      final api = Api();
      var jsonCity = await api.getCities(searchText);
      if (jsonCity != null) {
        City city = City.fromJson(jsonCity);
        setState(() {
          _cities.clear();
          _cities.add(CityTile(
              idOfCity: city.idOfCity,
              nameOfCity: city.name,
              country: city.country,
              lat: city.lat,
              lon: city.lon,
              description: city.description,
              icon: city.icon,
              temp: city.temp));
        });
      }
    }
  }

  Widget appBarTitle() {
    return Container(
        margin: _padding,
        padding: _padding,
        child: TextField(
          controller: _controller,
          style: TextStyle(
            color: Colors.white,
          ),
          decoration: InputDecoration.collapsed(
              hintText: "Search...", hintStyle: TextStyle(color: Colors.white)),
          onChanged: searchCities,
        ),
        decoration: BoxDecoration(
          border: Border(
            bottom: BorderSide(color: Colors.white, style: BorderStyle.solid),
          ),
        ));
  }

  void _clearSearch() {
    setState(() {
      this.appBarTitle();
      _controller.clear();
    });
  }

  Widget buildAppBar(BuildContext context) {
    return AppBar(
        elevation: 4.0,
        centerTitle: true,
        title: appBarTitle(),
        actions: <Widget>[
          IconButton(
            icon: Icon(
              Icons.close,
              color: Colors.white,
            ),
            onPressed: () {
              _clearSearch();
            },
          ),
        ]);
  }

  Widget _buildCurrentWidgets() {
    return ListView.builder(
      itemBuilder: (BuildContext context, int index) => _cities[index],
      itemCount: _cities.length,
    );
  }

  @override
  Widget build(BuildContext context) {
    assert(debugCheckHasMediaQuery(context));
    final listView = Padding(
      padding: EdgeInsets.only(
        left: 8.0,
        right: 8.0,
        bottom: 48.0,
      ),
      child: _buildCurrentWidgets(),
    );

    return Scaffold(appBar: buildAppBar(context), body: listView);
  }
}
