import 'package:flutter/material.dart';
import 'package:meta/meta.dart';

final _rowHeight = 50.0;

class CityTile extends StatelessWidget {
  final int idOfCity;
  final String nameOfCity;
  final String country;
  final double lat;
  final double lon;
  final int temp;
  final String icon;
  final String description;

  const CityTile({
    Key key,
    @required this.idOfCity,
    @required this.nameOfCity,
    @required this.country,
    @required this.lat,
    @required this.lon,
    @required this.temp,
    @required this.icon,
    @required this.description,
  })  : assert(idOfCity != null),
        assert(nameOfCity != null),
        assert(country != null),
        assert(lat != null),
        assert(lon != null),
        assert(temp != null),
        assert(icon != null),
        assert(description != null),
        super(key: key);

  @override
  Widget build(BuildContext context) {
    return Material(
      color: Colors.transparent,
      child: Container(
        height: _rowHeight,
        child: InkWell(
          onTap: () =>
              Navigator.pop(context, [idOfCity.toString(), nameOfCity]),
          child: Padding(
            padding: EdgeInsets.all(8.0),
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                new Expanded(
                  child: Align(
                    alignment: Alignment.centerLeft,
                    child: Text(
                      nameOfCity,
                      textAlign: TextAlign.left,
                      style: Theme.of(context).textTheme.headline,
                    ),
                  ),
                ),
                new Container(
                    alignment: Alignment.centerRight,
                    child: Image.network(
                        'http://openweathermap.org/img/w/$icon.png',
                        fit: BoxFit.cover)),
                Center(
                  child: Text(
                    temp.toString(),
                    textAlign: TextAlign.center,
                    style: Theme.of(context).textTheme.headline,
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
