import 'package:flutter/material.dart';
import 'package:meta/meta.dart';
import 'package:intl/intl.dart';

final _rowHeight = 50.0;

class ForecastCard extends StatelessWidget {
  const ForecastCard({
    Key key,
    @required this.forecast,
  }) : assert(forecast != null);

  final List<ForecastTile> forecast;

  Widget _buildForecastWidgets() {
    return ListView.builder(
      itemBuilder: (BuildContext context, int index) => forecast[index],
      itemCount: forecast.length,
    );
  }

  @override
  Widget build(BuildContext context) {
    final progress = Center(
      child: Container(
        color: Color(0xFFFFFF),
        height: 100.0,
        width: 100.0,
        child: CircularProgressIndicator(),
      ),
    );

    assert(debugCheckHasMediaQuery(context));
    final listView = Padding(
      padding: EdgeInsets.only(
        left: 8.0,
        right: 8.0,
        bottom: 48.0,
      ),
      child: _buildForecastWidgets(),
    );

    return forecast.isEmpty ? progress : listView;
  }
}

class ForecastTile extends StatelessWidget {
  final int date;
  final String description;
  final String icon;
  final int temp;

  ForecastTile({
    Key key,
    @required this.date,
    @required this.description,
    @required this.icon,
    @required this.temp,
  })  : assert(date != null),
        assert(description != null),
        assert(icon != null),
        assert(temp != null);

  /// Convert timestamp to MM:dd.
  String convertTimestamp(int timestamp) {
    return new DateFormat.MMMd()
        .format(new DateTime.fromMillisecondsSinceEpoch(timestamp * 1000));
  }

  @override
  Widget build(BuildContext context) {
    return Material(
      color: Colors.transparent,
      child: Container(
        height: _rowHeight,
        child: InkWell(
          onTap: () => {},
          child: Padding(
            padding: EdgeInsets.all(8.0),
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Align(
                  alignment: Alignment.centerLeft,
                  child: IntrinsicWidth(
                    stepWidth: 46.0,
                    child: Text(
                      convertTimestamp(date),
                      textAlign: TextAlign.left,
                      style: Theme.of(context).textTheme.headline,
                    ),
                  ),
                ),
                Expanded(
                  child: Padding(
                    padding: EdgeInsets.symmetric(horizontal: 8.0),
                    child: Align(
                      alignment: Alignment.centerLeft,
                      child: Text(
                        description,
                        textAlign: TextAlign.left,
                        style: Theme.of(context).textTheme.body2,
                      ),
                    ),
                  ),
                ),
                Container(
                    alignment: Alignment.centerRight,
                    child: Image.network(
                        'http://openweathermap.org/img/w/$icon.png',
                        fit: BoxFit.cover)),
                Align(
                  child: IntrinsicWidth(
                    stepWidth: 8.0,
                    child: Text(
                      temp.toString(),
                      textAlign: TextAlign.center,
                      style: Theme.of(context).textTheme.headline,
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
