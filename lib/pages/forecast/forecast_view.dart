import 'dart:async';
import 'package:flutter/material.dart';

import '../../data/forecast/forecast_data.dart';
import 'forecast_view_item.dart';
import 'package:weather_3/data/api.dart';

final _backgroundColor = Colors.blue[900];

class ForecastPage extends StatefulWidget {
  final String nameOfCity;
  final String idOfCity;

  const ForecastPage({
    Key key,
    @required this.nameOfCity,
    @required this.idOfCity,
  })  : assert(nameOfCity != null),
        assert(idOfCity != null),
        super(key: key);

  @override
  _ForecastPageState createState() => new _ForecastPageState();
}

class _ForecastPageState extends State<ForecastPage> {
  final _forecast = <ForecastTile>[];

  static const _icons = <String>[
    'assets/icons/ic_action_three.png',
    'assets/icons/ic_action_seven.png',
    'assets/icons/ic_flag_en.png',
    'assets/icons/ic_flag_ru.png',
  ];

  bool _langEN = false;
  bool _cntDaysThree = false;

  void _selectLang() {
    setState(() {
      _langEN = !_langEN;
      _forecast.clear();
      updateData();
    });
  }

  Widget _widgetLang() {
    return IconButton(
      icon: Image.asset(_langEN ? _icons[2] : _icons[3],
          height: 48.0, width: 48.0),
      onPressed: _selectLang,
    );
  }

  void _selectCnt() {
    setState(() {
      _cntDaysThree = !_cntDaysThree;
      _forecast.clear();
      updateData();
    });
  }

  Widget _widgetCnt() {
    return IconButton(
      icon: Image.asset(_cntDaysThree ? _icons[0] : _icons[1],
          height: 48.0, width: 48.0),
      onPressed: _selectCnt,
    );
  }

  void updateData() async {
    await _retrieveApiForecast();
  }

  @override
  Future<void> didChangeDependencies() async {
    super.didChangeDependencies();
    await _retrieveApiForecast();
  }

  Future<void> _retrieveApiForecast() async {
    final api = Api();
    final jsonUnits = await api.getForecast(new ForecastParams(
        idOfCity: widget.idOfCity,
        langEn: _langEN,
        cntDaysThree: _cntDaysThree));

    if (jsonUnits != null) {
      final forecasts = <Day>[];
      for (var unit in jsonUnits) {
        forecasts.add(Day.fromJson(unit));
      }
      setState(() {
        _forecast.clear();

        for (int i = 0; i < forecasts.length; i++) {
          _forecast.add(ForecastTile(
              date: forecasts[i].date,
              description: forecasts[i].description,
              icon: forecasts[i].icon,
              temp: forecasts[i].temp));
        }
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        elevation: 4.0,
        title: Text(
          widget.nameOfCity,
          style: TextStyle(
            color: Colors.white,
          ),
        ),
        backgroundColor: _backgroundColor,
        actions: <Widget>[
          _widgetLang(),
          _widgetCnt(),
        ],
      ),
      body: ForecastCard(forecast: _forecast),
    );
  }
}
