import 'package:flutter/material.dart';
import 'package:meta/meta.dart';

import '../../pages/forecast/forecast_view.dart';

final _rowHeight = 50.0;

class City extends StatelessWidget {
  final String idOfCity;
  final String nameOfCity;
  final String iconState;
  final int temp;

  const City({
    Key key,
    @required this.idOfCity,
    @required this.nameOfCity,
    @required this.iconState,
    @required this.temp,
  })  : assert(idOfCity != null),
        assert(nameOfCity != null),
        assert(iconState != null),
        assert(temp != null),
        super(key: key);

  /// Navigates to the [ForecastPage].
  void _navigateToForecast(BuildContext context) {
    Navigator.push(
      context,
      MaterialPageRoute(
          builder: (context) =>
              ForecastPage(nameOfCity: nameOfCity, idOfCity: idOfCity)),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Material(
      color: Colors.transparent,
      child: Container(
        height: _rowHeight,
        child: InkWell(
          onTap: () => _navigateToForecast(context),
          child: Padding(
            padding: EdgeInsets.all(8.0),
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Expanded(
                  child: Align(
                    alignment: Alignment.centerLeft,
                    child: Text(
                      nameOfCity,
                      textAlign: TextAlign.left,
                      style: Theme.of(context).textTheme.headline,
                    ),
                  ),
                ),
                Container(
                    alignment: Alignment.centerRight,
                    child: Image.network(
                        'http://openweathermap.org/img/w/$iconState.png',
                        fit: BoxFit.cover)),
                Center(
                  child: Text(
                    temp.toString(),
                    textAlign: TextAlign.center,
                    style: Theme.of(context).textTheme.headline,
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
