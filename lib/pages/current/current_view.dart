import 'dart:async';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../../data/current/current_data.dart';
import 'current_view_item.dart';
import 'package:weather_3/data/api.dart';
import '../../pages/add_city/city_add_view.dart';

final _backgroundColor = Colors.green[500];

///Current List
class CurrentsPage extends StatefulWidget {
  const CurrentsPage();

  @override
  _CurrentsPageState createState() => new _CurrentsPageState();
}

class _CurrentsPageState extends State<CurrentsPage> {
  final _scaffoldKey = new GlobalKey<ScaffoldState>();
  final _currents = <City>[];

  SharedPreferences prefs;
  List<String> _cities;

  @override
  Future<void> didChangeDependencies() async {
    super.didChangeDependencies();
    await _retrieveApiCurrents();
  }

  void updateData() async {
    await _retrieveApiCurrents();
  }

  Future<void> _retrieveApiCurrents() async {
    if (_cities == null || _cities.isEmpty) {
      prefs = await SharedPreferences.getInstance();
      _cities = prefs.getStringList('cities') ?? ['524901', '519690'];
    }
    final api = Api();
    final jsonUnits = await api.getCurrents(_cities);

    if (jsonUnits != null) {
      final currents = <Current>[];
      for (var unit in jsonUnits) {
        currents.add(Current.fromJson(unit));
      }
      setState(() {
        _currents.clear();
        for (int i = 0; i < currents.length; i++) {
          _currents.add(City(
              idOfCity: currents[i].idOfCity,
              nameOfCity: currents[i].nameOfCity,
              iconState: currents[i].lnk,
              temp: currents[i].temp));
        }
      });
    }
  }

  Widget _buildCurrentWidgets() {
    return ListView.builder(
      itemCount: _currents.length,
      itemBuilder: (BuildContext context, int index) {
        final item = _currents[index];

        return Dismissible(
          key: Key(item.idOfCity),
          onDismissed: (direction) {
            setState(() {
              _currents.removeAt(index);
              _cities.removeAt(index);

              prefs.setStringList('cities', _cities);
            });

            Scaffold.of(context).showSnackBar(
                SnackBar(content: Text(item.nameOfCity + " dismissed")));
          },
          background: Container(color: Colors.red),
          child: item,
        );
      },
    );
  }

  _navigate(BuildContext context) async {
    final result = await Navigator.push(
      context,
      MaterialPageRoute(builder: (context) => SearchPage()),
    );

    if (result != null) {
      _cities.add(result[0]);
      prefs.setStringList('cities', _cities);

      updateData();

      _scaffoldKey.currentState.showSnackBar(
          SnackBar(content: Text("${result[1]} added to list")));
    }
  }

  @override
  Widget build(BuildContext context) {
    final appBar = AppBar(
      elevation: 4.0,
      title: Text(
        'Weather 3',
        style: TextStyle(
          color: Colors.white,
        ),
      ),
      backgroundColor: _backgroundColor,
    );

    final progress = Center(
      child: Container(
        color: Color(0xFFFFFF),
        height: 100.0,
        width: 100.0,
        child: CircularProgressIndicator(),
      ),
    );

    assert(debugCheckHasMediaQuery(context));
    final listView = Padding(
      padding: EdgeInsets.only(
        left: 8.0,
        right: 8.0,
        bottom: 48.0,
      ),
      child: _buildCurrentWidgets(),
    );

    final fab = FloatingActionButton(
        elevation: 4.0,
        child: Icon(Icons.add),
        backgroundColor: _backgroundColor,
        onPressed: () {
          _navigate(context);
        });

    return Scaffold(
      key: _scaffoldKey,
      appBar: appBar,
      body: _currents.isEmpty ? progress : listView,
      floatingActionButton: Builder(
        builder: (BuildContext context) => fab,
      ),
    );
  }
}
