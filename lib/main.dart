import 'package:flutter/material.dart';
import 'pages/current/current_view.dart';

void main() {
  runApp(MaterialApp(
      title: 'Weather 3',
      theme: ThemeData(primarySwatch: Colors.blue),
      home: CurrentsPage()));
}
