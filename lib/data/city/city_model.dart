import 'package:meta/meta.dart';

class City {
  final int idOfCity;
  final String name;
  final String country;
  final double lat;
  final double lon;
  final int temp;
  final String icon;
  final String description;

  const City({
    @required this.idOfCity,
    @required this.name,
    @required this.country,
    @required this.lat,
    @required this.lon,
    @required this.temp,
    @required this.icon,
    @required this.description,
  })  : assert(idOfCity != null),
        assert(name != null),
        assert(country != null),
        assert(lat != null),
        assert(lon != null),
        assert(temp != null),
        assert(icon != null),
        assert(description != null);

  City.fromJson(Map jsonMap)
      : idOfCity = jsonMap['id'].toInt(),
        name = jsonMap['name'],
        country = jsonMap['sys']['country'],
        lat = jsonMap['coord']['lat'].toDouble(),
        lon = jsonMap['coord']['lon'].toDouble(),
        temp = jsonMap['main']['temp'].toInt(),
        icon = jsonMap['weather'][0]['icon'],
        description = jsonMap['weather'][0]['description'];
}
