import 'package:meta/meta.dart';

class Day {
  final int date;
  final String description;
  final String icon;
  final int temp;

  const Day({
    @required this.date,
    @required this.description,
    @required this.icon,
    @required this.temp,
  })  : assert(date != null),
        assert(description != null),
        assert(icon != null),
        assert(temp != null);

  Day.fromJson(Map jsonMap)
      : date = jsonMap['dt'],
        description = jsonMap['weather'][0]['description'],
        icon = jsonMap['weather'][0]['icon'],
        temp = jsonMap['temp']['day'].toInt();
}
