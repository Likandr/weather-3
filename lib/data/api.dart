import 'dart:async';
import 'dart:convert' show json, utf8;
import 'dart:io';

class Api {
  final String units = 'metric';
  final String appid = 'cd108bd5460fa6bf11a52a6e21cd3ad2';

  final HttpClient _httpClient = HttpClient();

  final String _url = 'api.openweathermap.org';
  final String _url2 = '/data/2.5/group';
  final String _url3 = '/data/2.5/forecast/daily';
  final String _url4 = '/data/2.5/weather';
  //****

  Future<List> getCurrents(List<String> cities) async {

    String citiesStr = '';
    for (String city in cities) {
      citiesStr += ',$city';
    }

    final uri = Uri.https(_url, _url2, {
      "id": citiesStr.substring(1),
      "units": units,
      "language": "en",
      "appid": appid
    });
    print('uri - ' + uri.toString());
    final jsonResponse = await _getJson(uri);
    print(jsonResponse);
    if (jsonResponse == null || jsonResponse['list'] == null) {
      print('Error retrieving units.');
      return null;
    }
    return jsonResponse['list'];
  }

  Future<List> getForecast(ForecastParams params) async {
    final uri = Uri.https(_url, _url3, {
      "units": units,
      "appid": appid,
      "id": params.idOfCity,
      "lang": params.getLangString(),
      "cnt": params.getCntString()
    });
    print('uri - ' + uri.toString());
    final jsonResponse = await _getJson(uri);
    print(jsonResponse);
    if (jsonResponse == null || jsonResponse['list'] == null) {
      print('Error retrieving units.');
      return null;
    }
    return jsonResponse['list'];
  }

  Future<Object> getCities(String query) async {
    final uri = Uri.https(_url, _url4, {"q": query, "units": units, "appid": appid});
    print('uri - ' + uri.toString());
    final jsonResponse = await _getJson(uri);
    print(jsonResponse);
    if (jsonResponse == null) {
      print('Error retrieving units.');
      return null;
    }

    return jsonResponse;
  }

  Future<Map<String, dynamic>> _getJson(Uri uri) async {
    try {
      final httpRequest = await _httpClient.getUrl(uri);
      final httpResponse = await httpRequest.close();
      if (httpResponse.statusCode != HttpStatus.OK) {
        return null;
      }
      final responseBody = await httpResponse.transform(utf8.decoder).join();
      return json.decode(responseBody);
    } on Exception catch (e) {
      print('$e');
      return null;
    }
  }
}

class ForecastParams {
  const ForecastParams({this.idOfCity, this.langEn, this.cntDaysThree});

  final String idOfCity;
  final bool langEn;
  final bool cntDaysThree;

  String getLangString() {
    return langEn ? "en" : "ru";
  }

  String getCntString() {
    return cntDaysThree ? "3" : "7";
  }
}