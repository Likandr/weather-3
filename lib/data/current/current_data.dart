import 'package:meta/meta.dart';

class Current {
  final String idOfCity;
  final String nameOfCity;
  final String lnk;
  final int temp;

  const Current({
    @required this.idOfCity,
    @required this.nameOfCity,
    @required this.lnk,
    @required this.temp,
  })  : assert(nameOfCity != null),
        assert(lnk != null),
        assert(temp != null);

  Current.fromJson(Map jsonMap)
      : idOfCity = jsonMap['id'].toString(),
        nameOfCity = jsonMap['name'],
        lnk = jsonMap['weather'][0]['icon'],
        temp = jsonMap['main']['temp'].toInt();
}
